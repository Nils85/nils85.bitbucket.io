# SQL compatibility guide
This documentation tries to list the level of (in)compatibility for SQL implementations between various database vendors.
The aim is to guide developers in choosing best keywords to write queries as much as possible "universal".

You can see [online version](http://nils85.bitbucket.io/sql-compatibility-guide/) or download files (standalone offline)

The pages are written with a simple HTML table and CSS stylesheet.  
Example of source code :

```
<html>
	<head>...</head>
	<body>
		<table>
			<tbody>
				...
				<tr>
					<td>FUNCTION(x,y,z)</td>
					<td><i>alternative</i></td>
					<td>SQL-92</td>
					<td title="MySQL" style="background-color: green">OK for Mysql</td>
					<td title="Oracle" style="background-color: red">Don't work</td>
					<td title="Postgres">Not tested (empty)</td>
					<td title="SQLserver" style="background-color: orange">Warning</td>
					<td title="SQLite"></td>
					...
				</tr>
				...
			</tbody>
			<thead>...</thead>
			<tfoot>...</tfoot>
		</table>
	</body>
</html>
```