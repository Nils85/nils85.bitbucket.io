create function SUBSTR(@x varchar, @y int, @z int = 0)
returns varchar
begin
	if @z = 0 return substring(@x, @y, len(@x) - @y);
	else return substring(@x, @y, @z);
end
go

create function LENGTH(@x varchar)
returns int
begin
	return len(@x);
end